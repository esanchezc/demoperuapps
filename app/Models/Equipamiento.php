<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipamiento extends Model
{
    use SoftDeletes;
    protected $table = 'equipamiento';
    protected $fillable = ['nombre'];
    protected $dates = ['deleted_at']; 
}
