<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comisario extends Model
{
    use SoftDeletes;
    protected $table = 'comisario';
    protected $fillable = ['nombres','apellidos'];
    protected $dates = ['deleted_at']; 
}
