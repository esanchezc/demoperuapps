<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comisario;
use App\Http\Requests\ComisarioRequest;
use DB;

class ComisarioController extends Controller
{
    public function index(){
        return view('admin.comisario');
    }

    public function getComisario(){
        $comisarios = Comisario::orderby('id','desc')->get();
        return response()->json($comisarios);
    }

    public function store(ComisarioRequest $request){

        DB::beginTransaction();
        try {
     
            $comisario = new Comisario($request->all());
            $comisario->save();

            DB::commit();
            return response()->json([
                'message' => 'Comisario Registrado'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function update(ComisarioRequest $request,$id){
       
        DB::beginTransaction();
        try {
            $comisario = Comisario::find($id);
            $comisario->fill($request->all());
            $comisario->save();

            DB::commit();
            return response()->json([
                'message' => 'Comisario Editado'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function delete($id){
       
        DB::beginTransaction();
        try {
            $comisario = Comisario::find($id);
            $comisario->delete();

            DB::commit();
            return response()->json([
                'message' => 'Comisario Eliminado'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }
}
