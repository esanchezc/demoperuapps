<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Equipamiento;
use App\Http\Requests\EquipamientoRequest;
use DB;

class EquipamientoController extends Controller
{
    public function index(){
        return view('admin.equipamiento');
    }

    public function getEquipamiento(){
        $equipamientos = Equipamiento::orderby('id','desc')->get();
        return response()->json($equipamientos);
    }

    public function store(EquipamientoRequest $request){
       
        DB::beginTransaction();
        try {
            $equipamiento = new Equipamiento($request->all());
            $equipamiento->save();

            DB::commit();
            return response()->json([
                'message' => 'Equipamiento Registrado'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function update(EquipamientoRequest $request,$id){
       
        DB::beginTransaction();
        try {
            $equipamiento = Equipamiento::find($id);
            $equipamiento->fill($request->all());
            $equipamiento->save();

            DB::commit();
            return response()->json([
                'message' => 'Equipamiento Editado'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function delete($id){
       
        DB::beginTransaction();
        try {
            $equipamiento = Equipamiento::find($id);
            $equipamiento->delete();

            DB::commit();
            return response()->json([
                'message' => 'Equipamiento Eliminado'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }
}
