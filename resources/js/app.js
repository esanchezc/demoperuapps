/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./sweetalert2');

import VeeValidate from 'vee-validate';
window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('login-component', require('./components/LoginComponent.vue').default);

//ADMIN
Vue.component('comisario-table', require('./components/comisario/TableComponent.vue').default);
Vue.component('equipamiento-table', require('./components/equipamiento/TableComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.mixin({
    methods: {
        headerToken(){
            return { 
                headers:  {
                    'Content-type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.token,
                    'Accept': 'application/json;odata=verbose',
                }
            };
        }
    }
});


const app = new Vue({
    el: '#app',
});
