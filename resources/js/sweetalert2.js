window.Vue = require('vue');
import Swal from 'sweetalert2/dist/sweetalert2.js'
import 'sweetalert2/src/sweetalert2.scss'

Vue.mixin({
    methods: {
        messageSuccess(text) {
            Swal.fire(
                'Éxito!',
                text,
                'success'
              )
        },
        messageError(text) {
            Swal.fire(
                'Error!',
                text,
                'error'
              )
        },
        messageConfirmation(otherFunc,text) {
            if (!text) {
                text = "No se puede revertir esta acción!";
            }
            Swal.fire({
                title: "¿Estas seguro?",
                text: text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar!',
                cancelButtonText: 'Cancelar'
              }).then((result) => {
                if (result.value) {
                    try{
                        otherFunc();
                   } catch(e) { }
                }
              })
        },
        loading(){
            Swal.fire({
                title: 'Cargando...',
                text: 'Por favor espere',
                allowOutsideClick: false,
                onOpen: () => {
                    Swal.showLoading();
                }
            })
        },
        closeLoading(){
            Swal.close();
        }
    }
});

