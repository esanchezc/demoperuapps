@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">LISTADO DE COMISARIOS</div>

                <div class="card-body">
                    <comisario-table></comisario-table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
