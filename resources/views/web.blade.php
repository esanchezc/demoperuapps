<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
</head>

<body>
    <header class="header d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 border-bottom shadow-sm">

        <div class="my-0 ml-md-auto">
            <form class="form-inline my-2 my-lg-0">

                <div class="input-group">
                    <img src="{{asset('images/search.png')}}" alt="">
                    <input class="form-control mr-sm-2 search" type="search" placeholder="¿Qué estás buscando?">
                </div>
            </form>
        </div>
        <div class="my-2 my-md-0 mr-md-3">
            <div class="navbar">
                {{-- <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button> --}}
                <a class="p-2 " href="#">
                    <img src="{{asset('images/icon_camara.png')}}" alt="" class="img">
                </a>
                <a class="p-2 " href="#">
                    <img src="{{asset('images/icon_face.png')}}" alt="" class="img">
                </a>
                <a class="p-2 text-white user" href="#">
                    Julieta Ortega
                    <img src="{{asset('images/Trazado473.svg')}}" alt="">
                </a>
                <a class="p-2 " href="#">
                    <img src="{{asset('images/user.png')}}" alt="" class="img">
                </a>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav menu">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Calendario</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Partners</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">E-Learning</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Material Informativo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Otros</a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
            <div class="col-md-3 responsive-none">
                <div class="row">
                    <div class="col-md-6 text-right">
                        <img src="{{asset('images/Grupo 1176.svg')}}" alt="">
                    </div>
                    <div class="col-md-6">
                        <p class="line-0"> <span class="numero">53948 </span>
                            <span class="visitas">Visitas</span></p>

                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="content-slider">
                    <div id="demo" class="carousel slide" data-ride="carousel">

                        <!-- Indicators -->
                        <ul class="carousel-indicators">
                            <li data-target="#demo" data-slide-to="0" class="active"></li>
                            <li data-target="#demo" data-slide-to="1"></li>
                            <li data-target="#demo" data-slide-to="2"></li>
                        </ul>

                        <!-- The slideshow -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="{{asset('images/slider.png')}}" alt="">
                            </div>
                            <div class="carousel-item">
                                <img src="{{asset('images/slider.png')}}" alt="">
                            </div>
                            <div class="carousel-item">
                                <img src="{{asset('images/slider.png')}}" alt="">
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                    </div>
                </div>
                <div class="cuadros">
                    <div class="item1">
                        <p>Opotunidad <br>
                            de Desarrollo</p>
                    </div>
                    <div class="item2">
                        <p>En Directo</p>
                    </div>
                    <div class="item3">
                        <p>Biométricos</p>
                    </div>
                    <div class="item4">
                        <span class="icon-4"></span>
                        <p>Programación <br>
                            SUM</p>
                    </div>
                    <div class="item5">
                        <p>Seguridad y <br>
                            Salud laboral</p>
                    </div>
                </div>

            </div>
            <div class="col-md-3">
                <div class="shadow  mb-2 bg-white rounded direct">
                    <div class="cabecera">
                        <div class="carp">
                            <img src="{{asset('images/Grupo 1238.svg')}}" alt="">
                        </div>
                        <h5>Directorio</h5>
                    </div>
                    <div class="content">
                        <div class="info">
                            <span><img src="{{asset('images/Grupo 1214.svg')}}" alt="" ></span>
                            <span class="descripcion">Numeros</span>
                        </div>
                        <div class="info">
                            <span><img src="{{asset('images/Grupo 1180.svg')}}" alt=""></span>
                            <span class="descripcion">¿Quiénes Somos?</span>
                        </div>
                        <div class="info">
                            <div>
                                <div class="info_icon">
                                    <img src="{{asset('images/Grupo 1181.svg')}}" alt="" class="base" >
                                    <img src="{{asset('images/Grupo 1233.svg')}}" alt="" class="subase" >
                                </div>
                                <span class="descripcion">SIG Normas y Procedimientos</span>
                            </div>
                        </div>
                        <div class="info">
                            <div>
                                <div class="info_icon">
                                    <img src="{{asset('images/Grupo 1182.svg')}}" alt="" class="base">
                                    <img src="{{asset('images/Grupo 1234.svg')}}" alt="" class="subase">
                                </div>
                                <span class="descripcion">Beneficios</span>
                            </div>
                        </div>
                        <div class="info">
                            <div>
                                <div class="info_icon">
                                    <img src="{{asset('images/Grupo 1183.svg')}}" alt="" class="base" width="41"
                                        height="41">
                                    <img src="{{asset('images/Grupo 1185.svg')}}" alt="" class="subase" width="23"
                                        height="17">
                                </div>
                                <span class="descripcion">Convenios Educativos</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shadow bg-white rounded">
                    <div class="cab content">
                        <div class="info">
                            <div>
                                <div class="info_icon cumple">
                                    <img src="{{asset('images/Grupo 1527.svg')}}" alt="" class="base" width="54"
                                        height="54">
                                </div>
                                <p class="person fz-18">Hoy de
                                    <br>
                                    Cumpleaños</p>
                            </div>
                        </div>
                    </div>
                    <div class="content scroll">
                        <div class="info">
                            <div>
                                <div class="info_icon">
                                    <img src="{{asset('images/Grupo 1242.png')}}" alt="" class="base" width="71"
                                        height="71">
                                </div>
                                <p class="person">Catarina Marcht
                                    <br>
                                    <span class="year">24 años</span></p>
                            </div>
                        </div>
                        <div class="info">
                            <div>
                                <div class="info_icon">
                                    <img src="{{asset('images/Grupo 1244.png')}}" alt="" class="base" width="71"
                                        height="71">
                                </div>
                                <p class="person">Elmer Pérez
                                    <br>
                                    <span class="year">26 años</span></p>
                            </div>
                        </div>
                        <div class="info">
                            <div>
                                <div class="info_icon">
                                    <img src="{{asset('images/Grupo 1246.png')}}" alt="" class="base" width="71"
                                        height="71">
                                </div>
                                <p class="person">Dora Palao
                                    <br>
                                    <span class="year">30 años</span></p>
                            </div>
                        </div>
                        <div class="info">
                            <div>
                                <div class="info_icon">
                                    <img src="{{asset('images/Grupo 1242.png')}}" alt="" class="base" width="71"
                                        height="71">
                                </div>
                                <p class="person">Catarina Marcht
                                    <br>
                                    <span class="year">24 años</span></p>
                            </div>
                        </div>
                        <div class="info">
                            <div>
                                <div class="info_icon">
                                    <img src="{{asset('images/Grupo 1244.png')}}" alt="" class="base" width="71"
                                        height="71">
                                </div>
                                <p class="person">Elmer Pérez
                                    <br>
                                    <span class="year">26 años</span></p>
                            </div>
                        </div>
                        <div class="info">
                            <div>
                                <div class="info_icon">
                                    <img src="{{asset('images/Grupo 1246.png')}}" alt="" class="base" width="71"
                                        height="71">
                                </div>
                                <p class="person">Dora Palao
                                    <br>
                                    <span class="year">30 años</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center empresa">
                <h2>¿Quiénes somos?</h2>
                <p>Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum
                    è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo
                    prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non
                    solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci
                    sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di
                    caratteri trasferibili “Letraset”, che contenevano passaggi del Lorem Ipsum, e più recentemente da
                    software di impaginazione come Aldus PageMaker, che includeva versioni del Lorem Ipsum.</p>
            </div>
        </div>
        <div class="row">
            <div class="gallery mb-5">
                <div class="item item1">
                    <img src="{{asset('images/Grupo 1656.svg')}}" alt=""">
                    <div class="text-white">
                        <h4>Galería de <br> Imágenes</h4>
                        <a href="#"><h5>Ver completa</h5></a>
                    </div>
                </div>
                <div class="item">
                    <img src="{{asset('images/Grupo 1641.png')}}" alt="">
                </div>
                <div class="item ">
                    <img src="{{asset('images/Grupo 1647.png')}}" alt="" >
                </div>
                <div class="item ">
                    <img src="{{asset('images/Grupo 1645.png')}}" alt="">
                </div>
               
                <div class="item ">
                    <img src="{{asset('images/yevveq.png')}}" alt="" >
                   
                </div>
                <div class="item ">
                    <img src="{{asset('images/Grupo 1649.png')}}" alt="">
                </div>
            </div>
        </div>
        
            <div class="contact  mb-5">
                <div class="row">
                <div class="col-md-4">
                   <div class="cabec">
                        <div class="info_icon">
                            <img src="{{asset('images/Grupo 1657.svg')}}" alt="" width="78" height="64">
                        </div>
                        <h3>Sucursales</h3>
                   </div>
                   <div class="content">
                    <div class="mb-2">
                            <span class="sede">Campus La Molina </span><br>
                            <span class="gris">AvenidaLa Molina 190,</span><br>
                            <span class="gris">Ate, Lima Perú.</span>
                        </div>
                        <div class="mb-2">
                            <span class="sede">Campus Callao</span><br>
                            <span class="gris">Avenida Maquinarias 6015,</span><br>
                            <span class="gris">Carmen de la Legua, Callao Perú</span>
                        </div>
                        <div class="mb-2">
                            <span class="sede">Centro Trujillo</span><br>
                            <span class="gris">Jirón Grau 400 Centro Comercial</span><br>
                            <span class="gris">Boulevard 3er piso, Trujillo Perú</span>
                        </div>
                   </div>
                </div>
                <div class="col-md-8 mapa" >
                    <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
                    <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
                    <img src="{{asset('images/Rectángulo 30.png')}}" alt="" >
                </div>
            </div>
          
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
</body>

</html>
