@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <h5>Bienvenido</h5>
                    <h6 class="text-center">ADMINISTRAR</h6>
                    <hr>
                    <div class="text-center">
                        <a href="{{route('comisario.index')}}" class="btn btn-info">Comisarios</a>
                        <a href="{{route('equipamiento.index')}}"  class="btn btn-info">Equipamientos</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
