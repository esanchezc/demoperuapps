<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/web', 'WebController@index')->name('web');
// Auth::routes();
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');

// Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['before'=>'jwt.auth']], function(){
 
});
Route::group(['middleware' => ['web']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/comisario', 'admin\ComisarioController@index')->name('comisario.index');
    Route::get('/equipamiento', 'admin\EquipamientoController@index')->name('equipamiento.index');
});


