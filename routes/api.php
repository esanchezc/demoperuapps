<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('logout', 'Auth\AuthController@logout');
    Route::post('refresh', 'Auth\AuthController@refresh');
    Route::post('me', 'Auth\AuthController@me');

});

Route::group(['middleware' => 'jwt.auth'], function () {
    //RUTAS DE COMISARIO
    Route::post('comisario/get', 'admin\ComisarioController@getComisario');
    Route::post('comisario/store', 'admin\ComisarioController@store');
    Route::put('comisario/update/{id}', 'admin\ComisarioController@update');
    Route::delete('comisario/delete/{id}', 'admin\ComisarioController@delete');

    //RUTAS DE EQUIPAMIENTO
    Route::post('equipamiento/get', 'admin\EquipamientoController@getEquipamiento');
    Route::post('equipamiento/store', 'admin\EquipamientoController@store');
    Route::put('equipamiento/update/{id}', 'admin\EquipamientoController@update');
    Route::delete('equipamiento/delete/{id}', 'admin\EquipamientoController@delete');
});
